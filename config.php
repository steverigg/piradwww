<!doctype html>
<HTML>

<HEAD>
  <title>piRad configuration</title>
  <link rel="stylesheet" href="styles.css" type="text/css">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>

  <script>
      function divRefresh() {
        $('#refresh_stream').load('refresh.php');
      }
  </script>

  <script>
    var auto_refresh = setInterval(
      function ()
      {
        divRefresh();
      }, 
      3000); // refresh period (ms)
  </script>


</HEAD>


<body onLoad="divRefresh()">
  <div class="main_panel">
    <div class="header">
      <img src="piradconf.png" class="header_img">
    </div>

    <br/>
    <br/>
    <br/>   
    <div id="refresh_stream"> </div>
    <br/>
    <br/>

    <div class="main_layout">

      <table width="400" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
        <tr>
          <form name="frmAddStream" method="post" action="addStream.php">
            <td>
              <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
                <tr>
                  <td colspan="4" align="left"><strong>Add new stream</strong></td>
                </tr>
                <tr>
                  <td width="100">Description</td>
                  <td width="6">:</td>
                  <td colspan="2" width="280"><input name="txtDescription" type="text" id="txtDescription"></td>
                </tr>
                <tr>
                  <td>Stream URL</td>
                  <td>:</td>
                  <td colspan="2"><input name="txtStreamUrl" type="text" id="txtStreamUrl"></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="50"><input type="submit" name="Submit" value="Add stream"></td>
                </tr>
              </table>
            </td>
          </form>
        </tr>
      </table>
    </div>

    <br/>   


    <?php
      include_once 'sockets.php';

      $host="localhost"; // Host name
      $username="piradmin"; // Mysql username
      $password="piradmin"; // Mysql password
      $db_name="piraddb"; // Database name
      $tbl_name="sources"; // Table name

      mysql_connect($host, $username, $password) or die(mysql_error());
      mysql_select_db($db_name) or die(mysql_error());
      $sql = "select * from $tbl_name";
      $db_data = mysql_query($sql) or die(mysql_error());
      $rowcnt = mysql_num_rows($db_data);
      
    ?>


    <table id="streamListHolder" class="stream_list_table">
      <TR>
        <TD colspan="5" align="center"> S T R E A M   L I S T </td>
      </TR>


      <TR>
        <TD>
          <form name="frmStationList" method="post" action="">

            <table id="streamList" class="stream_list_table">

              <TR>
                <TD/>
                <TD/>
                <TD align="left">
                  <INPUT name="play" type="submit" id="play" value="Play">
                  <INPUT name="stop" type="submit" id="stop" value="Stop">
                  <INPUT name="delete" type="submit" id="delete" value="Delete">
                </td>
                <TD/>
              </tr>

              <TR class="stream_list_row_header">
                <TH style="width:5%">#</TH>
                <TH style="width:5%">ID</TH>
                <TH style="width:35%">DESCRIPTION</TH>
                <TH>URL</TH>
              </TR>
                          

              <?php      
                $rowid = 1;
                while($info = mysql_fetch_array($db_data))
                {
              ?>

              <TR class="stream_list_row">
                <TD class="stream_list_cell">
                  <input name="checkbox[]" type="checkbox" id="checkbox[]" value="<? echo ($info['id'].','.$rowid); ?>">
                </TD>                            
                <TD class="stream_list_cell"> <? echo $info['id']; ?> </TD>
                <TD class="stream_list_cell"> <? echo $info['description']; ?> </TD>
                <TD class="stream_list_cell"> <? echo $info['url']; ?> </TD>
              </TR>

              <?php
                $rowid++;
                }      
              ?>      


              <?php              
                $checkbox = $_POST['checkbox'];
                $delete = $_POST['delete'];
                $play = $_POST['play'];
                $stop = $_POST['stop'];

                if($delete) {
                  for($i = 0; $i < count($checkbox); $i++) {
                    $chunks = explode(",", $checkbox[$i]);
                    $del_id = $chunks[0];
                    $sql = "DELETE FROM $tbl_name WHERE id=$del_id";
                    $result = mysql_query($sql);
                  }
                
                  if($result){
                    doReload();
                    echo "<meta http-equiv=\"refresh\" content=\"0;URL=index.php\">";
                  }
                }
                
                if($stop) {
                  doStop();
                }
                
                if($play) {
                  if(empty($checkbox))
                  {
                    doPlay();
                  }
                  else
                  {
                    $chunks = explode(",", $checkbox[0]);
                    $sel_id = $chunks[1];
                    doPlaySelection($sel_id);
                  }
                }
                
              ?>
      
            </table>
          </form>
         </TD>
      </TR>      
    </table>   
  </div>  
</body>
