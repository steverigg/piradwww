<!doctype html>
<HTML>

<HEAD>
  <title>piRad</title>
  <link rel="stylesheet" href="mstyles.css" type="text/css">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>

  <script>
      function divRefresh() {
        $('#refresh_stream').load('refresh.php');
      }
  </script>

  <script>
    var auto_refresh = setInterval(
      function ()
      {
        divRefresh();
      }, 
      3000); // refresh period (ms)
  </script>


</HEAD>


<body onLoad="divRefresh()">
  <div class="main_panel">

    <div id="fixed_top">

      <div id="header">
        <img src="pirad.png" id="header_img">
      </div>
  
      <div id="refresh_stream"> </div>
  
  
      <?php
        include_once 'sockets.php';
  
        $host="localhost"; // Host name
        $username="piradmin"; // Mysql username
        $password="piradmin"; // Mysql password
        $db_name="piraddb"; // Database name
        $tbl_name="sources"; // Table name
  
        mysql_connect($host, $username, $password) or die(mysql_error());
        mysql_select_db($db_name) or die(mysql_error());
        $sql = "select * from $tbl_name";
        $db_data = mysql_query($sql) or die(mysql_error());
        $rowcnt = mysql_num_rows($db_data);

        if(isset($_POST['stop'])) {
          doStop();
        }
        elseif(isset($_POST['play'])) {
          doPlay();
        }                       
        elseif(isset($_GET['station'])) {
          doPlaySelection($_GET['station']);
        }
        
      ?>
  
  
      <div id="controls">
        <form name="controls_form" method="post" action="">
          <input name="play" type="submit" id="play" value="Play">
          <input name="stop" type="submit" id="stop" value="Stop">
        </form>
      </div>
    
    </div>

    <div id="stations">

      <?php
        $idx = 0;      
        while($info = mysql_fetch_array($db_data)) :
          $idx++;
     
      ?>

      <a href="index.php?station=<?php echo $idx; ?>">
        <div class="station">
          <?php echo $info['description']; ?>
        </div>
      </a>

      
      <?php
        endwhile;
      ?>

    </div>

    <div id="footer">
    </div>

  </div>  
</body>
