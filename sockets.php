
<?php


function openSocket()
{
  /* Get the port for the service. */
  $service_port = 50006;

  /* Get the IP address for the target host. */
  $address = 'localhost';

  /* Create a TCP/IP socket. */
  $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
  if ($socket === false) 
  {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
    return null;
  } 
  else 
  {
//    echo "socket created!";
    // do nothing for now
  }

  $result = socket_connect($socket, $address, $service_port);
  if ($result === false) 
  {
    echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
    return null;
  } 
  else 
  {
    //echo "socket connected!";
    // do nothing for now
    return $socket;
  }
}

function closeSocket($aSocket)
{
  socket_close($aSocket);
}

function doReload()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "LOAD", 4);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doPlay()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "PLAY", 4);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doPlaySelection($id)
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "SEL ". $id, 8);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doStop()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "STOP", 4);
  while ($out = socket_read($socket, 2048)) {
    
    //echo $out;
  }
  closeSocket($socket);
}

function getInfo()
{
  $socket = openSocket();
  if (!$socket) return;

  $info = "";
  socket_write($socket, "INFO", 4);
  while ($out = socket_read($socket, 1024)) {
    $info .= $out;
  }
  closeSocket($socket);
  if (strlen($info)==0) $info = "Nothing - stopped";
  return $info;
  
}



?>
